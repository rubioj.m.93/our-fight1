package dev.rubio.models;

import java.io.Serializable;
import java.util.Objects;

public class Doctor implements Serializable {

    private int id;
    private String name;
    private String specialty;
    private String focus;
    private String gender;
    private boolean available;
    private int phone;


    public Doctor(int id, String name, String specialty, String focus, String gender, boolean available, int phone){
        super();
    }

    public Doctor(int id, String name, String specialty, String focus, String gender, boolean available){
        this.id = id;
        this.name = name;
        this.specialty = specialty;
        this.focus = focus;
        this.gender = gender;
        this.available = available;
        this.phone = phone;

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getFocus() {
        return focus;
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", specialty='" + specialty + '\'' +
                ", focus='" + focus + '\'' +
                ", gender='" + gender + '\'' +
                ", available=" + available +
                ", phone=" + phone +
                '}';
    }
}

