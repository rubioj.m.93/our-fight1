package dev.rubio.data;

import dev.rubio.models.Doctor;

import java.util.List;

public interface DoctorDao {
    public List<Doctor> getAllDoctors();
}
