package dev.rubio.data;

import dev.rubio.models.Doctor;
import dev.rubio.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DoctorDaoImpl implements DoctorDao {
    private Logger logger = LoggerFactory.getLogger(DoctorDaoImpl.class);

    @Override
    public List<Doctor> getAllDoctors() {
        List<Doctor> doctors = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from doctor");

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String specialty = resultSet.getString("specialty");
                String focus = resultSet.getString("focus");
                String gender = resultSet.getString("gender");
                boolean available = resultSet.getBoolean("available");
                int phone = resultSet.getInt("phone");
                Doctor doctor = new Doctor(id, name,specialty,focus,available,phone);
                doctors.add(doctor);
            }
            logger.info("selecting all Doctors from database -" + doctors.size() + "doctors retrieved");
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return doctors;
    }
}
