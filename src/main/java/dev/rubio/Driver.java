package dev.rubio;

import dev.rubio.controllers.AuthController;
import dev.rubio.controllers.DoctorController;
import io.javalin.Javalin;


public class Driver {


    public static void main(String[] args) {
        Javalin app = Javalin.create().start(7000); //establishing a connection with Javalin
        //app.get("/", ctx -> ctx.result("Hello World"));
        DoctorController doctorController = new DoctorController();
        AuthController authController = new AuthController();

        app.get("/doctors", doctorController::handleGetDoctorsRequest); // json method converts object to JSON




    }
}




