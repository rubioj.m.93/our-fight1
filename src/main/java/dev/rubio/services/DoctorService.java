package dev.rubio.services;

import dev.rubio.data.DoctorDaoImpl;
import dev.rubio.data.DoctorDao;
import dev.rubio.models.Doctor;

import java.util.List;

public class DoctorService {

    private DoctorDao doctorDao = new DoctorDaoImpl();

    public List<Doctor> getAll(){
        return doctorDao.getAllDoctors(); //omg, this method call was driving me nuts
    }
}
