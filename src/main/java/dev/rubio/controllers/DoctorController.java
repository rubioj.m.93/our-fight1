package dev.rubio.controllers;

import dev.rubio.services.DoctorService;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DoctorController {
    private Logger logger = LoggerFactory.getLogger(DoctorController.class);
    private DoctorService service = new DoctorService();

    public void handleGetDoctorsRequest(Context ctx){
        logger.info("getting all doctors");
        ctx.json(service.getAll());
    }


}
