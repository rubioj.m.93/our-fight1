

package dev.rubio.controllers;

import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthController {
    private Logger logger = LoggerFactory.getLogger(AuthController.class);

    public void authenticateLogin(Context ctx){
        String user = ctx.formParam("username");
        String pass = ctx. formParam("password");
        logger.info(user +" attempted login");
        if(user!=null && user.equals("rubio")){
            logger.info("successful login");
            ctx.header("Authorization", "you-can-do-it");
            ctx.status(200);
            return;
        }
    }
}
